package com.lessons;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        String[] userStrings = new String[16];
        int lastIndexOfUserInputString = 0;
        double averageLengthOfUserSentences = 0;

        Scanner consoleInput = new Scanner(System.in);

        System.out.println("Ведите строки. Для окончания ввода введите пустую строку.");

        while (true) {
            String inputUserString = consoleInput.nextLine();

            if (inputUserString.equals("")) {
                break;
            }

            if (userStrings.length == lastIndexOfUserInputString) {
                String[] strArr = userStrings;
                int newArrayUserStringsSize = userStrings.length + 16;
                userStrings = new String[newArrayUserStringsSize];
                System.arraycopy(strArr, 0, userStrings, 0, strArr.length);
            }
            userStrings[lastIndexOfUserInputString++] = inputUserString;
        }

        for (int i = 0; i < lastIndexOfUserInputString; i++) {
            averageLengthOfUserSentences += userStrings[i].length();
        }
        averageLengthOfUserSentences /= lastIndexOfUserInputString;

        System.out.println("Размер данных строк меньше среднего: ");
        for (int i = 0; i < lastIndexOfUserInputString; i++) {
            if (userStrings[i].length() < averageLengthOfUserSentences) {
                System.out.println(userStrings[i]);
            }
        }

    }
}