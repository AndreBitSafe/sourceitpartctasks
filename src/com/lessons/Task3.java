package com.lessons;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        String[] userStrings = new String[16];
        String[] keywords = {"Night", "Color", "Milk"};
        int lastIndexOfUserInputString = 0;

        Scanner consoleInput = new Scanner(System.in);

        System.out.println("Ведите строки используя ключевые слова \"Night\", \"Color\", \"Milk\".\n" +
                " Для окончания ввода введите пустую строку.");
        // Getting the rows from the console and write them to the "userStrings" array
        while (true) {
            String inputUserString = consoleInput.nextLine();

            if (inputUserString.equals("")) {
                break;
            }

            if (userStrings.length == lastIndexOfUserInputString) {
                String[] strArr = userStrings;
                int newArrayUserStringsSize = userStrings.length + 16;
                userStrings = new String[newArrayUserStringsSize];
                System.arraycopy(strArr, 0, userStrings, 0, strArr.length);
            }
            userStrings[lastIndexOfUserInputString++] = inputUserString;
        }


        for (int i = 0; i < lastIndexOfUserInputString; i++) {
            String inputUserStringInForI = userStrings[i];

            // Check each row of the "userStrings" array for keywords
            for (int j = 0; j < keywords.length; j++) {
                String keywordInForJ = keywords[j];
                if (inputUserStringInForI.contains(keywordInForJ)) {
                    String[] mayBeContainsKeywordArray = inputUserStringInForI
                            .replaceAll("\\p{P}", "")
                            .split(" ");

                    for (String element : mayBeContainsKeywordArray) {
                        if (element.equals(keywordInForJ)) {
                            System.out.println("Данная строка содержит ключевое слово: \"" + inputUserStringInForI + "\"");
                        }
                    }

                }
            }
        }

    }
}
