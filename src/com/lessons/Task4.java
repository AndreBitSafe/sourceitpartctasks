package com.lessons;


import java.util.Scanner;

public class Task4 {
    //4** Ввести с консоли несколько предложений.
    // Удалить в них все слова заданной длины,
    // начинающиеся на согласную букву.
    public static void main(String[] args) {
        String[] userStrings = new String[16];
        int lastIndexOfUserInputString = 0;
        int wordDeleteSize = 0;

        Scanner consoleInput = new Scanner(System.in);

        System.out.print("Укажите размер удаляемых слов: ");
        wordDeleteSize = Integer.parseInt(consoleInput.nextLine());

        System.out.println("Ведите несколько предложений.\nДля окончания ввода введите пустую строку.");

        while (true) {
            String inputUserString = consoleInput.nextLine();

            if (inputUserString.equals("")) {
                break;
            }

            if (userStrings.length == lastIndexOfUserInputString) {
                String[] strArr = userStrings;
                int newArrayUserStringsSize = userStrings.length + 16;
                userStrings = new String[newArrayUserStringsSize];
                System.arraycopy(strArr, 0, userStrings, 0, strArr.length);
            }
            userStrings[lastIndexOfUserInputString++] = inputUserString;
        }

        for (int i = 0; i < lastIndexOfUserInputString; i++) {
            StringBuilder inputUserStringInForI = new StringBuilder(userStrings[i]);
            boolean stringHasBeenChanged = false;
            String[] inputUserStringInWords = userStrings[i]
                    .replaceAll("\\p{P}", "")
                    .split(" ");

            for (String word : inputUserStringInWords) {
                if (word.length() == wordDeleteSize && word.matches("^(?ui:[бвгджзйклмнпрстфхцчшщы]).*")) {
                    // For English words: word.matches("^(?i:[bcdfjhgklmnpqrstvwxz]).*")
                    stringHasBeenChanged = true;
                    int startIndex = inputUserStringInForI.indexOf(word);
                    int endIndex =   startIndex + word.length();
                    inputUserStringInForI.replace(startIndex, endIndex, "\"Delete word\"");
                }
            }

            if (stringHasBeenChanged) {
                System.out.println(inputUserStringInForI.toString());
            }
        }

    }
}
