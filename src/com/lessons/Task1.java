package com.lessons;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        String[] userStrings = new String[16];
        int lastIndexOfUserInputString = 0;
        int indexOfBiggestSentence = 0;
        int sizeOfBiggestSentence = 0;

        Scanner consoleInput = new Scanner(System.in);

        System.out.println("Ведите строки. Для окончания ввода введите пустую строку.");

        while (true) {
            String inputUserString = consoleInput.nextLine();

            if (inputUserString.equals("")) { break; }

            if (userStrings.length == lastIndexOfUserInputString) {
                String[] strArr = userStrings;
                int newArrayUserStringsSize = userStrings.length + 16;
                userStrings = new String[newArrayUserStringsSize];
                System.arraycopy(strArr, 0, userStrings, 0, strArr.length);
            }
            userStrings[lastIndexOfUserInputString++] = inputUserString;
        }

        for (int i = 0; i < lastIndexOfUserInputString; i++) {
            int stringSize = userStrings[i].length();
            if (stringSize >= sizeOfBiggestSentence) {
                sizeOfBiggestSentence = stringSize;
                indexOfBiggestSentence = i;
            }
        }

        System.out.println("Самая длинная строка: " + "\"" + userStrings[indexOfBiggestSentence] + "\"");
        System.out.println(sizeOfBiggestSentence + " - символа(ов).");

    }
}
